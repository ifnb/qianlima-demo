package com.qianlima.jpademo.dto;

import io.github.linpeilie.annotations.AutoMapMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author：allms
 * @date: 2023/4/7 16:35
 */
@Data
@NoArgsConstructor
@AutoMapMapper
public class UserDTO {
    private Long id;
    private String name;
    private Integer age;
    private String roleName;

    public UserDTO(Long id, String name, Integer age, String roleName) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.roleName = roleName;
    }
}
