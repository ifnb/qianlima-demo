package com.qianlima.jpademo.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @description:
 * @author：allms
 * @date: 2023/4/7 15:57
 */
@Entity
@Table(name = "sys_role")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;


}
