# 安装libreoffice

1、查看当前yum支持当前Linux安装的LibreOffice版本
yum search libreoffice

2、查看libreoffice安装包基本信息
yum info libreoffice

3、安装libreoffice
yum install -y libreoffice

4、启动libreoffice

/usr/bin/soffice --headless --accept="socket,host=127.0.0.1,port=8100;urp;" --nofirststartwizard &

查看是否启动成功：
查看端口：
`netstat -anp |grep "8100"`
查看进程：
`ps -ef | grep 进程id`

5、设置开机自启

```shell
# 进入rc.local
vi /etc/rc.local`
# 尾部添加命令：
`/usr/bin/soffice --headless --accept="socket,host=127.0.0.1,port=8100;urp;" --nofirststartwizard &
```

# 安装libreoffice字体

由于默认的libreoffice字体小于100个，会导致某些字体加载不正常，所以安装字体是必要的。

（1）在/usr/share/fonts/下创建一个目录存放Windows字体

```shell
# mkdir /usr/share/fonts/winfonts/
```

（2）将字体上传到创建好的目录并解压

```shell
# cd /usr/share/fonts/winfonts/
# tar xf Windows_fonts.tar.gz
```

（3）建立字体索引信息，更新字体缓存

```shell
# mkfontscale
# mkfontdir
# fc-cache -fv
```

（4）让字体生效

```shell
# source /etc/profile
```

（5）再次查看字体

```shell
# fc-list  |wc -l
```

将上面3~5步合并下，一条命令搞定

```shell
# mkfontscale && mkfontdir && fc-cache -fv && source /etc/profile && fc-list  |wc -l 
```

# 解决中文乱码问题

因为 centos 7 默认使用的是英文，所以需要执行命令：

yum groupinstall "fonts"

安装成功后,

打开

vim /etc/locale.conf

按键 i 进入编辑模式, 把内容改为

LANG="zh_CN.UTF-8"

保存配置文件

source /etc/locale.conf

-- 然后重启你的jar服务即可，若多次尝试还是无法显示中文，需要重启机器。



- 转载：[关于libreoffice在centos7上转pdf遇到中文乱码的问题_xujingcheng123的博客-CSDN博客_centos libreoffice 乱码](https://blog.csdn.net/xujingcheng123/article/details/84643021)
- 转载：[Linux-安装Windows字体 - 别来无恙- - 博客园 (cnblogs.com)](https://www.cnblogs.com/yanjieli/p/10119900.html)
- 转载：[Linux环境下安装LibreOffice_erhuobuer的博客-CSDN博客_linux安装libreoffice](https://blog.csdn.net/erhuobuer/article/details/107758279)

