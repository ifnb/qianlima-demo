package com.qianlima.jpademo;

import com.qianlima.jpademo.dto.UserDTO;
import com.qianlima.jpademo.entity.User;
import com.qianlima.jpademo.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class JpaDemoApplicationTests {

    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        userService.test();
    }

    @Test
    void test2(){
        List<UserDTO> userById = userService.findUserById(1L);
    }

    @Test
    void youya(){
        userService.youyaixefa();
    }
}
