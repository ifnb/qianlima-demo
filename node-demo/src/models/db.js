// 导入mysql模块
import mysql from 'mysql';

// 封装一个函数，创建并返回一个数据库连接对象
export function createConnection() {
    // 定义数据库连接信息
    const connectionConfig = {
        host: '159.75.102.95',
        port: '3306',
        user: 'root',
        password: 'qianlima',
        database: 'jpa-demo'
    };
    // 创建并返回一个连接对象
    return mysql.createConnection(connectionConfig);
}

// 封装一个函数，根据用户名查询用户信息，并返回一个Promise对象
export function queryUserByUsername(username) {
    console.log(`username:${username}`)
    // 返回一个Promise对象
    return new Promise((resolve, reject) => {
        // 创建一个数据库连接对象
        const connection = createConnection();
        // 连接数据库
        connection.connect();
        // 定义查询语句
        const querySql = `select * from sys_user where name = ?`;
        // 执行查询语句，传入用户名作为参数
        connection.query(querySql, [username], (error, results) => {
            // 如果查询过程出现异常，调用reject函数传递错误信息
            if (error) {
                reject(error);
                return;
            }
            // 如果查询结果为空，调用resolve函数传递null
            if (results.length === 0) {
                resolve(null);
                return;
            }
            // 如果查询结果不为空，调用resolve函数传递用户信息
            resolve(results[0]);
        });
        // 关闭数据库连接
        connection.end();
    });
}