package com.qianlima.jpademo.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @description: 用户对象
 * @author：allms
 * @date: 2023/4/7 15:53
 */
@Entity
@Table(name = "sys_user")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer age;

    // 使用@ManyToOne注解来定义和Role实体类的多对一关系
    @ManyToOne
    private Role role;


}
