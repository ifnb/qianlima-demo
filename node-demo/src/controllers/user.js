// 导入db.js模块
import { queryUserByUsername } from '../models/db.js';

// 定义一个函数，根据用户名查询用户信息，并返回JSON格式的响应
async function getUserByUsername(req, res) {
    // 获取请求参数中的用户名
    const username = req.query.username;
    // 如果没有提供用户名，返回400状态码和错误信息
    if (!username) {
        res.status(400).json({ error: 'Missing username parameter' });
        return;
    }
    // 使用await语法等待Promise对象的结果
    try {
         // 调用封装在db.js中的函数，获取一个Promise对象
        const user = await queryUserByUsername(username);
        // 如果结果为null，表示用户不存在，返回404状态码和错误信息
        if (!user) {
            res.status(404).json({ error: 'User not found' });
            return;
        }
        // 如果结果不为null，表示用户存在，返回200状态码和用户信息
        res.status(200).json(user);
    } catch (err) {
        // 如果出现异常，返回500状态码和错误信息
        res.status(500).json({ error: err.message });
    }
}

// 导出getUserByUsername函数
export { getUserByUsername };