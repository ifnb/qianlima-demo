package cn.alllms.certificate.doc;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 *  用于docx文件的格式转换
 * @author josxy
 */
public class AsposeWordUtil {

	/**
	 * Word转PDF操作
	 * 
	 * @param sourcerFile 源文件
	 * @param targetFile  目标文件
	 */
	public static boolean WordToPdf(String sourcerFile, String targetFile) {
		// 验证License 若不验证则转化出的pdf文档会有水印产生
		if (!getLicense()) {
			return false;
		}
		try {
			long old = System.currentTimeMillis();
			// 新建一个空白pdf文档
			File file = new File(targetFile);
			FileOutputStream os = new FileOutputStream(file);
			// sourcerFile是将要被转化的word文档
			Document doc = new Document(sourcerFile);
			// 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
			doc.save(os, SaveFormat.PDF);
			os.close();
			long now = System.currentTimeMillis();
			System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒"); // 转化用时
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 判断是否有授权文件 如果没有则会认为是试用版，转换的文件会有水印
	 * 
	 * @return
	 */
	public static boolean getLicense() {
		boolean result = false;
		try {
			InputStream is = AsposeWordUtil.class.getClassLoader().getResourceAsStream("license.xml");
			License aposeLic = new License();
			aposeLic.setLicense(is);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// 测试Word转PDF
	public static void main(String[] args) {
		String startFile = "C:\\Users\\josxy\\Desktop\\a.docx";

		String endFile = "C:\\Users\\josxy\\Desktop\\a1.pdf";

		WordToPdf(startFile, endFile);
	}

}
