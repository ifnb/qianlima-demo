package cn.alllms.certificate;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import cn.alllms.certificate.doc.AsposeWordUtil;
import cn.alllms.certificate.doc.DocOperator;
import cn.alllms.certificate.doc.domain.CertificateData;
import cn.alllms.certificate.path.domain.CertificateTempPaths;
import cn.alllms.certificate.pdf.PdfConverter;
import cn.alllms.certificate.properties.PropertiesReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.XmlException;

/** 
 * 证件图片生成器
 * @author xuhaojin
 * @version [版本号, 2020年3月24日]
 */
public class CertificateGenerator {

	public static void generate(CertificateData data) throws IOException, XmlException {
		// 获取生成证书预设参数
		Properties properties = PropertiesReader.getProperties("config\\certificate.properties");
		String officePath = properties.getProperty("libreoffice.path");
		String imageSuffix = properties.getProperty("certificate.image.suffix");
		String imageScaleStr = properties.getProperty("certificate.image.scale");

		Float imageScale = null;
		try {
			imageScale = Float.valueOf(imageScaleStr);
		} catch (NumberFormatException e) {
			// image scale转换失败
		}

		// 获取证书模板
		File docxTemplate = new File("D:\\template.docx");

		// 生成临时文件路径
		CertificateTempPaths tempPaths = CertificateTempPaths.newInstance("docx", "pdf", imageSuffix);

		// 复制证书模板，将字段替换为自定义数据
		String tempDocxPath = tempPaths.getTempDocPathName();
		DocOperator.toCumstomDoc(docxTemplate, tempDocxPath, data);

		// 将doc模板转成pdf
		String tempPdfPath = tempPaths.getTempPdfPathName();
		AsposeWordUtil.WordToPdf(tempDocxPath,tempPdfPath);
		// 将pdf转成证书图片
		PdfConverter.toImageUsingPdfbox(tempPdfPath, tempPaths.getTempImagePathName(), imageSuffix, imageScale);

		// 删除生成证书使用的临时文件
		tempPaths.deleteTempFiles();
	}

	public static void generate(CertificateData data, String imageSuffix) {

	}

	public static String addBlankSpace(String text) {
		StringBuffer sb = new StringBuffer();

		if (text == null) {
			return null;
		}

		char[] chars = text.toCharArray();

		String regex = "[\u4E00-\u9FA5]{1}";
		for (char aChar : chars) {
			String str = aChar + "";
			if (StringUtils.isBlank(str)) {
				continue;
			}

			sb.append(aChar);

			if (str.matches(regex)) {
				sb.append(" ");
			}
		}

		return sb.toString();
	}

	public static void main(String[] args) throws IOException, XmlException {
		// TODO 本项目为copy项目
//		CertificateData data = new CertificateData();
//		data.put(new CertificateField("产品名称", "大方西红柿", 12));
//		data.put(new CertificateField("产品编号", "P2998883989999", 12));
//		data.put(new CertificateField("所在区块", "158847", 12));
//		data.put(new CertificateField("唯一Hash", "48k92ks9gmm29sm9764js1488kkdksu88s3", 12));
//		data.put(new CertificateField("上链时间", "2021-09-09 11:32:31", 12));
//		data.put(new CertificateField("上链企业", "广州市溯源科技有限公司", 12));
//		data.put(new CertificateField("机构代码", "888888888888888888", 12));
//		generate(data);
		// 合成图片
//		String erweima = "C:\\Users\\j1osxy\\Downloads\\certificate-generator-master\\certificate-generator-master\\certificate-generator\\企业证书\\二维码.png";
//		ImgUtil.pressImage(
//				FileUtil.file("C:\\Users\\j1osxy\\Downloads\\certificate-generator-master\\certificate-generator-master\\certificate-generator\\企业证书\\5e7406c6.jpg"),
//				FileUtil.file("C:\\Users\\j1osxy\\Downloads\\certificate-generator-master\\certificate-generator-master\\certificate-generator\\企业证书\\合成.jpg"),
//				ImgUtil.read(FileUtil.file(erweima)), //水印图片
//				10, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
//				420, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
//				0.9f
//		);

		System.out.println();
	}

}
