// 导入express模块
import express from 'express';
// 导入user.js模块
import { getUserByUsername } from '../controllers/user.js';

// 创建一个Router对象
const router = express.Router();

// 定义路由，使用封装在user.js中的函数作为回调函数
router.get('/user', getUserByUsername);

// 导出Router对象
export default router;