package com.qianlima.jpademo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author：allms
 * @date: 2023/4/7 17:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserMapDTO extends HashMap<String, Object> implements Map<String, Object> {

}
