// 导入模块
import express from 'express';
// 导入router.js模块
import router from './routers/router.js';

// 创建应用
const app = express();

// 使用use方法将Router对象挂载到'/api'路径上
app.use('/api', router);

export default app;