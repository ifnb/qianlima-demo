package com.qianlima.jpademo.service;

import com.qianlima.jpademo.dto.UserDTO;
import com.qianlima.jpademo.dto.UserMapDTO;
import com.qianlima.jpademo.entity.User;
import com.qianlima.jpademo.repository.UserRepository;
import io.github.linpeilie.Converter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author：allms
 * @date: 2023/4/7 16:06
 */
@Service
@Slf4j
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;

    @PersistenceContext
    private EntityManager em;

    private Converter converter;

    @SuppressWarnings("all")
    public List<UserDTO> findUserById(Long id) {
        Query dataQuery = em.createNativeQuery("select u.id, u.name, u.age, r.name as roleName from sys_user u join sys_role r on u.role_id = r.id");
        List<Object[]> queryResultList = dataQuery.getResultList();
        List<UserDTO> resultList = queryResultList.stream()
                //将每个Object[]数组映射为一个User对象
                .map(obj -> new UserDTO(((BigInteger) obj[0]).longValue(), (String) obj[1], (int) obj[2], (String) obj[3])).collect(Collectors.toList());
        log.info("list:{}", resultList);
        return resultList;
    }

    public void save() {
        User user = new User();
        user.setName("浅浅");
        user.setAge(18);
        User save = userRepository.save(user);
        log.info("user=>{}", save);
    }

    public void findByRoleName() {
        List<User> admin = userRepository.findByRoleName("admin");
        log.info("user=>{}", admin);
    }

    public void findUserByRoleName() {
        List<User> admin = userRepository.findUserByRoleName("admin");
        log.info("user=>{}", admin);
    }

    public void findUserAndRole() {
//        List<Object[]> list = userRepository.findUserAndRole();
//        List<UserDTO> resultList = new ArrayList<>();
//        list.forEach(l -> {
//            BigInteger id = (BigInteger) l.get("id");
//            String name = (String) l.get("name");
//            Integer age = (Integer) l.get("age");
//            String roleName = (String) l.get("roleName");
//            UserDTO userDTO = new UserDTO(id.longValue(), name, age, roleName);
//            resultList.add(userDTO);
//        });
//        log.info("resultList=>{}", resultList);
    }

    public void youyaixefa() {
        List<Object[]> list = userRepository.findUserAndRole();
        List<UserDTO> resultList = list.stream()
                //将每个Object[]数组映射为一个User对象
                .map(obj -> new UserDTO(((BigInteger) obj[0]).longValue(), (String) obj[1], (int) obj[2], (String) obj[3])).collect(Collectors.toList());
        log.info("list:{}", resultList);
    }

    public void test() {
        List<UserMapDTO> localUser = userRepository.findLocalUser();
        List<UserDTO> entityList = new ArrayList<>();
        for (UserMapDTO map : localUser) {
            UserDTO entity = new UserDTO();
            //把BigInteger转换成Long
            map.put("id", ((BigInteger) map.get("id")).longValue());
            BeanUtils.copyProperties(entity, map);
            entityList.add(entity);
        }
        log.info("entityList=>{}", entityList);
    }

    public void findUserRoleDTO() {
        List<UserDTO> userRoleDTO = userRepository.findUserRoleDTO();
        log.info("userRoleDTO=>{}", userRoleDTO);
    }
}
